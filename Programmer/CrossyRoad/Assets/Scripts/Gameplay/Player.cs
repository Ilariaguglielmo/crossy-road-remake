using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour
{
    #region Variables & Properties
    GameManager gameManagerRef;
    Rigidbody rb;
    Renderer rnd;
    RaycastHit hit;
    Vector3 nextDir;
    Vector3 curPosition;

    bool canMove;
    float cosPiFourths = Mathf.Sqrt(2) / 2;
    float xAxisValue;
    float zAxisValue;

    [Space(20)]
    [Header("Player Parameters")]
    [Tooltip("Player movement speed")]
    [SerializeField] float movingSpeed;
    [Tooltip("Player movement speed")]
    [SerializeField] float rotationSpeed;
    [Tooltip("Player jump strength")]
    [SerializeField] float jumpForce;
    [Tooltip("How long will jump last for before being pushed back to the ground")]
    [SerializeField] float jumpWindowTime;

    [Space(20)]
    [Header("Collision Parameters")]
    [Tooltip("Which kind of obstacles should the player be looking out for")]
    [SerializeField] LayerMask raycastChannel;

    public GameManager GameManager { set { gameManagerRef = value; } }
    public bool CanMove { set { canMove = value; } }
    #endregion


    #region Mono
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rnd = GetComponent<Renderer>();
        curPosition = transform.position;
    }


    private void Update()
    {
        if (!canMove) return;

        Vector3 nextPosition = new Vector3(curPosition.x, .5f, curPosition.z) + nextDir;

        //If a new position has been identified, move and rotate towards it
        if (transform.position != nextPosition)
        {
            transform.position = Vector3.MoveTowards(transform.position, nextPosition, movingSpeed * Time.deltaTime);
            if (nextDir != Vector3.zero)
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(nextDir), rotationSpeed * Time.deltaTime);
            transform.localScale = new Vector3(transform.localScale.x, nextDir.z != 0 ? (-.3f*Mathf.Cos(2f*(nextPosition.z - transform.position.z + Mathf.PI/2f)) + .7f) : (-.3f * Mathf.Cos(2f * (nextPosition.x - transform.position.x + Mathf.PI / 2f)) + .7f), transform.localScale.z);
        }    
        else if (rb.velocity == Vector3.zero)
        {
            //If nextDir hasn't already been reset, reset it
            if (nextDir != Vector3.zero)
            {
                nextDir = Vector3.zero;
                curPosition = transform.position;
            }

            //WASD movement
            if (zAxisValue != 0)
                zAxisValue = 0;
            if (xAxisValue != 0)
                xAxisValue = 0;

            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.Space))
                zAxisValue = 1f;
            else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
                zAxisValue = -1f;
            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
                xAxisValue = 1f;
            else if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
                xAxisValue = -1f;

            if (zAxisValue != 0 && (zAxisValue == 1 ? !CheckForHit(Vector3.forward) : !CheckForHit(Vector3.back)))
            {
                nextDir.z = zAxisValue;
                StartCoroutine(SimulateJump());
            }
            else if (xAxisValue != 0 && (xAxisValue == 1 ? !CheckForHit(Vector3.right) : !CheckForHit(Vector3.left)))
            {
                nextDir.x = xAxisValue;
                StartCoroutine(SimulateJump());
            }
        }
    }
    #endregion


    #region Methods
    /// <summary>
    /// Simulates a jumping animation and sends information to game manager about the next player location
    /// </summary>
    /// <returns></returns>
    IEnumerator SimulateJump()
    {
        //GameManager checks new player position to update game status
        gameManagerRef.CheckNewPlayerPosition(transform.position + nextDir);

        rb.AddForce(0f, jumpForce, 0f);
        yield return new WaitForSeconds(jumpWindowTime);
        rb.AddForce(0f, -jumpForce, 0f);
    }


    /// <summary>
    /// Checks for raycast collision in given direction
    /// </summary>
    /// <param name="direction">Which direction to look towards</param>
    /// <returns></returns>
    bool CheckForHit(Vector3 direction)
    {
        if (Physics.Raycast(transform.position, direction, 1f, raycastChannel))
            return true;
        return false;
    }
    #endregion
}
