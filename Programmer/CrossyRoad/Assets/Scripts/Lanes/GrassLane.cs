using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassLane : Lane
{
    #region Variables & Properties
    [Space(20)]
    [Header("Spawnable Lane Items")]
    [Tooltip("Which items can be spawned in this lane (and their spawn rate)")]
    [SerializeField] LaneItem[] laneSpawnableItems;
    #endregion


    #region Mono

    #endregion


    #region Methods
    public override void InitializeLane(bool[] clearPaths, int spawnProbability)
    {
        laneItems = new List<GameObject>();
        int probability;

        for (int i = 0; i < clearPaths.Length; i++)
        {
            probability = Random.Range(1, 101);

            if (probability <= spawnProbability)
            {
                if (!clearPaths[i])
                    laneItems.Add(Instantiate(laneSpawnableItems[PickSpawnedItem()].item, new Vector3(i - clearPaths.Length / 2, 0f, transform.position.z), Quaternion.identity));
            }
        }   
    }


    int PickSpawnedItem()
    {
        int index = 0;
        float random = UnityEngine.Random.value;

        while (random > 0)
        {
            random -= (float)laneSpawnableItems[index].probability / 100;
            index++;
        }
        index--;

        return index;
    }


    public override void DestroyLane()
    {
        if (laneItems != null)
        {
            for (int i = 0; i < laneItems.Count; i++)
                Destroy(laneItems[i]);
        }

        Destroy(gameObject);
    }
    #endregion
}
