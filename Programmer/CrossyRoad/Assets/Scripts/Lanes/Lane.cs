using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct LaneItem
{
    public GameObject item;
    public int probability;
}


public class Lane : MonoBehaviour
{
    #region Variables & Properties
    public LaneType type;
    protected List<GameObject> laneItems;
    #endregion


    #region Mono

    #endregion


    #region Methods
    public virtual void InitializeLane(bool[] clearPaths, int spawnProbability)
    {

    }


    public virtual void DestroyLane()
    {
        
    }
    #endregion
}
