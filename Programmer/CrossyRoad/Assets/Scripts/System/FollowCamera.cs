using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    #region Variables & Properties
    Vector3 cameraOffset;
    Vector3 velocity = Vector3.zero;
    Vector3 followPosition;
    Vector3 panningPoint;
    Transform targetToFollow;

    bool isInitialized;

    [Space(20)]
    [Header("Camera Settings")]
    [Tooltip("How often (seconds) should camera update panning position")]
    [SerializeField] float cameraUpdateTime;
    [Tooltip("Approximately the time it will take to reach the target")]
    [SerializeField] float smoothingTime;
    [Tooltip("How many lanes per second will the camera be panning forward")]
    [SerializeField] float panningSpeed;
    #endregion


    #region Mono
    //Camera always follows panning point
    private void Update()
    {
        if (!isInitialized) return;

        followPosition = targetToFollow.position + cameraOffset;
        transform.position = Vector3.SmoothDamp(transform.position, panningPoint, ref velocity, smoothingTime);
    }


    //If player gets out of camera view, game over
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Player>() != null)
            Debug.Log("Player is out of camera, GAME OVER!");
    }
    #endregion


    #region Methods
    /// <summary>
    /// Sets up follow camera, assigning target positions, starting the update cycle
    /// </summary>
    /// <param name="target">Target to follow</param>
    public void InitializeCamera(Transform target)
    {
        targetToFollow = target;

        cameraOffset = transform.position - targetToFollow.position;
        panningPoint = cameraOffset;
        StartCoroutine(CameraPanning());

        isInitialized = true;
    }


    IEnumerator CameraPanning()
    {
        while (true)
        {
            if (targetToFollow.position.z > panningPoint.z - cameraOffset.z)
                panningPoint = new Vector3(targetToFollow.position.x + cameraOffset.x, targetToFollow.position.y + cameraOffset.y, targetToFollow.position.z + cameraOffset.z);
            
            panningPoint = new Vector3(targetToFollow.position.x + cameraOffset.x, targetToFollow.position.y + cameraOffset.y, panningPoint.z + panningSpeed*cameraUpdateTime);
            yield return new WaitForSeconds(cameraUpdateTime);
        }
    }
    #endregion
}
