using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Variables & Properties
    Player playerRef;

    int travelDistance;
    int maxTravelDistance;
    int coins;

    [Space(20)]
    [Header("Prefabs References")]
    [Tooltip("The player prefab reference")]
    [SerializeField] GameObject prefab_Player;

    [Space(20)]
    [Header("GameObjects References")]
    [Tooltip("The follow camera reference")]
    [SerializeField] FollowCamera followCameraRef;
    [Tooltip("The map generator reference")]
    [SerializeField] MapGenerator mapGeneratorRef;
    [Tooltip("The gameUI reference")]
    [SerializeField] GameUI gameUIRef;
    #endregion


    #region Mono
    private void Start()
    {
        InitializeNewGame();
    }
    #endregion


    #region Methods
    /// <summary>
    /// Starts a new game, initializing the player and the camera
    /// </summary>
    void InitializeNewGame()
    {
        playerRef = Instantiate(prefab_Player, new Vector3(0f, .5f, 0f), Quaternion.identity).GetComponent<Player>();
        playerRef.GameManager = this;
        gameUIRef.UpdateDistanceScore(0);
    }


    public void StartGame()
    {
        followCameraRef.InitializeCamera(playerRef.transform);
        playerRef.CanMove = true;
    }


    /// <summary>
    /// Checks if the player lane is a new one and updates all information
    /// </summary>
    /// <param name="newPlayerPosition">The new player position</param>
    public void CheckNewPlayerPosition(Vector3 newPlayerPosition)
    {
        if (Mathf.Abs(Mathf.RoundToInt(newPlayerPosition.x)) >= 5)
            Debug.Log("Player out of bounds!");

        travelDistance = Mathf.RoundToInt(newPlayerPosition.z);
        if (travelDistance > maxTravelDistance)
        {
            maxTravelDistance = travelDistance;
            gameUIRef.UpdateDistanceScore(maxTravelDistance);
            mapGeneratorRef.UpdateLanesQueue(travelDistance);
        }
    }
    #endregion
}
