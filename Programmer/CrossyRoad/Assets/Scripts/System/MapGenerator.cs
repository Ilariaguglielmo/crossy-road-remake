using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum LaneType
{
    GRASS,
    ROAD,
    RIVER,
    RAILWAY
}


struct ClearPath
{
    public Vector3Int coords;
    public bool justTurned;
    public int initialX;
    public int range;
}


public class MapGenerator : MonoBehaviour
{
    #region Variables & Properties
    Lane lastLane = null;
    Queue<Lane> lanesQueue = new Queue<Lane>();
    List<ClearPath> clearPaths;

    const int walkableLanes = 9;
    bool[] clearPathRoutes = new bool[walkableLanes];

    [Space(20)]
    [Header("Generation Parameters")]
    [Tooltip("How long should the map be at all times in the z axis")]
    [SerializeField] int lanesSpawnRange;
    [Tooltip("How long should the spawn point be in the z axis starting from lane zero")]
    [SerializeField] int spawnLength;
    [Range(0, 3)]
    [Tooltip("How many pathways should always be kept clear")]
    [SerializeField] int clearPathsCount;
    [Range(0, 3)]
    [Tooltip("How far on the x axis can a path move from its starting point")]
    [SerializeField] int clearPathsMaxSpreadRange;
    [Tooltip("Probability of an obstacle of being spawned if it's in a suitable location")]
    [SerializeField] int obstaclesSpawnProbability;

    [Space(20)]
    [Header("Debug Parameters")]
    [Tooltip("Shows debug information regarding the map generator")]
    [SerializeField] bool debugMode;

    [Space(20)]
    [Header("Lanes References")]
    [Tooltip("All the references to all spawnable lanes")]
    [SerializeField] Lane[] lanes;
    #endregion


    #region Mono
    //Generate spawn area on start
    private void Start()
    {
        GenerateSpawn();
    }
    #endregion


    #region Methods
    /// <summary>
    /// Generates the initial spawn location
    /// </summary>
    void GenerateSpawn()
    {
        for (int z = -lanesSpawnRange; z <= lanesSpawnRange; z++)
        {
            if (z < spawnLength - 1 || lastLane == null)
            {
                lastLane = Instantiate(lanes[0], new Vector3(0f, 0f, z), Quaternion.identity).GetComponent<Lane>();
                lanesQueue.Enqueue(lastLane);
                if (z >= 0)
                {
                    CalculateClearPathways();
                }
                    
            }
            else
                GenerateNextLane(z);
        }
    }


    /// <summary>
    /// Updates lanes queue, popping the last one and pushing a new one
    /// </summary>
    /// <param name="newLanePosition"></param>
    public void UpdateLanesQueue(int newLanePosition)
    {
        Lane lane = lanesQueue.Dequeue();

        switch (lane.type)
        {
            case LaneType.GRASS:
                lane.GetComponent<GrassLane>().DestroyLane();
                break;

            default:
                Destroy(lane.gameObject);
                break;
        }

        GenerateNextLane(newLanePosition + lanesSpawnRange);
    }


    void GenerateNextLane(int z)
    {
        lastLane = Instantiate(lanes[Random.Range(0, lanes.Length)], new Vector3(0f, 0f, z), Quaternion.identity).GetComponent<Lane>();
        lanesQueue.Enqueue(lastLane);

        CalculateClearPathways();
        PlaceObstacles(lastLane);
    }


    void CalculateClearPathways()
    {
        //If no pathway exists, initialize them
        if (clearPaths == null)
        {
            clearPaths = new List<ClearPath>();
            int xCoord;

            for (int i = 0; i < clearPathsCount; i++)
            {
                xCoord = (i == 0 ? 0 : i == 1 ? 2 : -2);

                ClearPath clearPath = new ClearPath { coords = new Vector3Int(xCoord, 0, 0), justTurned = false, initialX = xCoord, range = clearPathsMaxSpreadRange };
                clearPaths.Add(clearPath);

                clearPathRoutes[clearPaths[i].coords.x + walkableLanes/2] = true;
            }

            if (debugMode)
            {
                for (int i = 0; i < clearPathRoutes.Length; i++)
                {
                    if (!clearPathRoutes[i])
                        Debug.DrawRay(new Vector3(i - walkableLanes / 2, clearPaths[0].coords.y, clearPaths[0].coords.z), Vector3.up * .3f, Color.black, 3600f);
                }
            }
        }
        else
        {
            //Sort paths based on distance
            clearPaths = SortPathPoints(clearPaths);
            for (int i = 0; i < clearPathRoutes.Length; i++)
                clearPathRoutes[i] = false;

            int probability = 0;
            bool mustTurn = false;

            //For each path point, advance and try to turn
            for (int i = 0; i < clearPaths.Count; i++)
            {
                if (Mathf.Abs(clearPaths[i].coords.x) == walkableLanes / 2)
                {
                    if (clearPaths[i].coords.x > 0)
                    {
                        if (!clearPathRoutes[(clearPaths[i].coords.x - 1) + walkableLanes / 2])
                            TurnPath(-1, i);
                        else 
                            TurnPath(0, i);
                    }
                    else
                    {
                        if (!clearPathRoutes[(clearPaths[i].coords.x + 1) + walkableLanes / 2])
                            TurnPath(1, i);
                        else
                            TurnPath(0, i);
                    }
                }
                else
                {
                    if (clearPathRoutes[(clearPaths[i].coords.x) + walkableLanes / 2])
                        mustTurn = true;
                    else
                    {
                        if (mustTurn)
                            mustTurn = false;

                        probability = Random.Range(1, 101);
                    }

                    if ((probability < 30 || mustTurn) && (clearPaths[i].coords.x - 1 >= clearPaths[i].initialX - clearPaths[i].range) && !clearPathRoutes[(clearPaths[i].coords.x - 1) + walkableLanes / 2])
                        TurnPath(-1, i);
                    else if ((probability < 60 || mustTurn) && (clearPaths[i].coords.x + 1 <= clearPaths[i].initialX + clearPaths[i].range) && !clearPathRoutes[(clearPaths[i].coords.x + 1) + walkableLanes / 2])
                        TurnPath(1, i);
                    else
                        TurnPath(0, i);
                }
            }

            if (debugMode)
            {
                for (int i = 0; i < clearPathRoutes.Length; i++)
                {
                    if (!clearPathRoutes[i])
                        Debug.DrawRay(new Vector3(i - walkableLanes / 2, clearPaths[0].coords.y, clearPaths[0].coords.z), Vector3.up * 0.3f, Color.black, 3600f);
                }
            }
        }
    }


    void TurnPath(int direction, int i)
    {
        clearPathRoutes[clearPaths[i].coords.x + walkableLanes / 2] = true;

        if (direction == 1 && !clearPaths[i].justTurned)
        {
            if (debugMode)
            {
                Debug.DrawRay(clearPaths[i].coords, Vector3.up, Color.red, 3600f);
                Debug.DrawRay(clearPaths[i].coords + Vector3.forward, Vector3.up, Color.red, 3600f);
                Debug.DrawRay(clearPaths[i].coords + Vector3.forward + Vector3.right, Vector3.up, Color.red, 3600f);

                Debug.DrawRay(clearPaths[i].coords, Vector3.forward, Color.blue, 3600f);
                Debug.DrawRay(clearPaths[i].coords + Vector3.forward, Vector3.right, Color.blue, 3600f);
            }

            ClearPath tmpClearPath = new ClearPath {coords = new Vector3Int(clearPaths[i].coords.x + 1, clearPaths[i].coords.y, clearPaths[i].coords.z + 1), justTurned = true, initialX = clearPaths[i].initialX, range = clearPaths[i].range };
            clearPaths[i] = tmpClearPath;

            clearPathRoutes[clearPaths[i].coords.x + walkableLanes / 2] = true;
        }
        else if (direction == -1 && !clearPaths[i].justTurned)
        {
            if (debugMode)
            {
                Debug.DrawRay(clearPaths[i].coords, Vector3.up, Color.red, 3600f);
                Debug.DrawRay(clearPaths[i].coords + Vector3.forward, Vector3.up, Color.red, 3600f);
                Debug.DrawRay(clearPaths[i].coords + Vector3.forward + Vector3.left, Vector3.up, Color.red, 3600f);

                Debug.DrawRay(clearPaths[i].coords, Vector3.forward, Color.blue, 3600f);
                Debug.DrawRay(clearPaths[i].coords + Vector3.forward, Vector3.left, Color.blue, 3600f);
            }

            ClearPath tmpClearPath = new ClearPath { coords = new Vector3Int(clearPaths[i].coords.x - 1, clearPaths[i].coords.y, clearPaths[i].coords.z + 1), justTurned = true, initialX = clearPaths[i].initialX, range = clearPaths[i].range };
            clearPaths[i] = tmpClearPath;

            clearPathRoutes[clearPaths[i].coords.x + walkableLanes / 2] = true;
        }
        else if (direction == 0 || clearPaths[i].justTurned)
        {
            if (debugMode)
            {
                Debug.DrawRay(clearPaths[i].coords, Vector3.up, Color.red, 3600f);
                Debug.DrawRay(clearPaths[i].coords + Vector3.forward, Vector3.up, Color.red, 3600f);

                Debug.DrawRay(clearPaths[i].coords, Vector3.forward, Color.blue, 3600f);
            }

            ClearPath tmpClearPath = new ClearPath { coords = new Vector3Int(clearPaths[i].coords.x, clearPaths[i].coords.y, clearPaths[i].coords.z + 1), justTurned = false, initialX = clearPaths[i].initialX, range = clearPaths[i].range };
            clearPaths[i] = tmpClearPath;
        }
    }


    /// <summary>
    /// Sorts path points based on shortest distance first
    /// </summary>
    /// <param name="clearPaths">Paths to be sorted</param>
    /// <returns></returns>
    List<ClearPath> SortPathPoints(List<ClearPath> clearPaths)
    {
        List<ClearPath> sortedPaths = new List<ClearPath>();
        int minDistance = walkableLanes;
        int minVectorIndex = 0;
        int vectorCount = clearPaths.Count;

        for (int i = 0; i < vectorCount; i++)
        {
            for (int j = 0; j < vectorCount - i; j++)
            {
                if (Mathf.Abs(clearPaths[j].coords.x) <= minDistance)
                {
                    minVectorIndex = j;
                    minDistance = (int)Mathf.Abs(clearPaths[j].coords.x);
                }
            }
            sortedPaths.Add(clearPaths[minVectorIndex]);
            clearPaths.RemoveAt(minVectorIndex);
            minDistance = walkableLanes;
        }

        return sortedPaths;
    }


    void PlaceObstacles(Lane lane)
    {
        if (lane.type == LaneType.GRASS)
            lane.GetComponent<GrassLane>().InitializeLane(clearPathRoutes, obstaclesSpawnProbability);
    }
    #endregion
}
