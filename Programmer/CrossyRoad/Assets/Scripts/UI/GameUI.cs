using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class GameUI : MonoBehaviour
{
    #region Variables & Properties
    [SerializeField] GameManager gameManagerRef;
    [SerializeField] GameObject ui_Menu;
    [SerializeField] GameObject ui_Game;
    [SerializeField] TMP_Text text_DistanceScore;
    #endregion


    #region Mono

    #endregion


    #region Methods
    /// <summary>
    /// Updates max player distance UI
    /// </summary>
    /// <param name="newDistance">New player distance</param>
    public void UpdateDistanceScore(int newDistance)
    {
        text_DistanceScore.text = newDistance.ToString();
    }


    public void SwitchToGameUI()
    {
        ui_Menu.SetActive(false);
        ui_Game.SetActive(true);
        gameManagerRef.StartGame();
    }
    #endregion
}
